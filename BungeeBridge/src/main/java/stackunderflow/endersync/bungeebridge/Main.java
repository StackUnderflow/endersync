package stackunderflow.endersync.bungeebridge;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import stackunderflow.endersync.bungeebridge.networking.Networking;
import stackunderflow.endersync.bungeebridge.utils.Crypto;
import stackunderflow.endersync.bungeebridge.utils.StringFormatter;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class Main extends Plugin {

    // ================================     VARS

    // References
    public static Main INSTANCE;
    private Configuration config;

    // Settings
    private boolean DEBUG = false;
    private String pluginChatPrefix = "§l§7» §l§3Ender§l§fSync§7-§6BungeeBridge {success}§l§7*";
    private String version;

    // State
    private List<UUID> playersOccupied;



    // ================================     CONSTRUCTOR & SINGLETON

    public Main() {
        if (Main.INSTANCE == null) {
            Main.INSTANCE = this;
        }
        setPlayersOccupied(new ArrayList<>());
    }



    // ================================     BUNGEECORD OVERRIDES

    @Override
    public void onEnable() {

        log("Loading config...");

        // Setup config.
        try {
            loadConfig();
        } catch (IOException e) {
            logError("Could not load config.yml file!");
            e.printStackTrace();
        }

        // Set values.
        setDEBUG(getConfig().getBoolean("plugin.debug", false));
        setPluginChatPrefix(getConfig().getString("plugin.prefix", getPluginChatPrefix()));
        Crypto.KEY = Arrays.copyOf(getConfig().getString("networking.password").getBytes(), 16);

        log("Enabling networking ...");

        new Networking();
        Networking.INSTANCE.setup();
        Networking.INSTANCE.start();

        log("Ready! :-)");

    }


    @Override
    public void onDisable() {

        log("Disabling networking ...");

        Networking.INSTANCE.shutdown();

    }


    /*
     * Utility methods for handling the config.yml file.
     */
    public void loadConfig() throws IOException {

        if (!getDataFolder().exists()) getDataFolder().mkdir();
        File file = new File(getDataFolder(), "config.yml");
        if (!file.exists()) {
            InputStream in = getResourceAsStream("config.yml");
            Files.copy(in, file.toPath());
        }

        setConfig(ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml")));
    }
    public void saveConfig() throws IOException {
        ConfigurationProvider.getProvider(YamlConfiguration.class).save(getConfig(), new File(getDataFolder(), "config.yml"));
    }


    // ================================     HELPERS

    /*
     * Output formatted log information to the console.
     */
    public void log(String str) {
        str = "{prefix} §r" + str;
        System.out.println(new StringFormatter(str).getSTR());
    }
    public void logError(String str) {
        str = "{prefix} §l§cERROR: §r" + str;
        System.out.println(new StringFormatter(str).getSTR());
    }
    public void logWarning(String str) {

        // RET: No debug.
        if (!isDEBUG()) return;

        str = "{prefix} §l§eWARNING: §r" + str;
        System.out.println(new StringFormatter(str).getSTR());

    }
    public void logDebug(String str) {

        // RET: No debug.
        if (!isDEBUG()) return;

        str = "{prefix} §l§6DEBUG: §r" + str;
        System.out.println(new StringFormatter(str).getSTR());

    }

}
