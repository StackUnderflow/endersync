package stackunderflow.endersync.bungeebridge.utils;


import net.md_5.bungee.api.config.ServerInfo;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/*
 * Utilities for messaging between Bukkit & BungeeCord.
 */
public class Messaging {

    /*
     * Sends a message to a server.
     */
    public static void sendToBukkit(String channel, String message, ServerInfo server) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF(channel);
            out.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.sendData("endersync:return", stream.toByteArray());
    }

}
