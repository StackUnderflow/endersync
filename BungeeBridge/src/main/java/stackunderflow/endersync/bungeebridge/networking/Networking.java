package stackunderflow.endersync.bungeebridge.networking;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.CharsetUtil;
import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersync.bungeebridge.Main;
import stackunderflow.endersync.bungeebridge.utils.Crypto;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;


/*
 * Handles networking (sockets).
 * It is used to establish a connection to and use the BungeeBridge.
 */
@Getter
@Setter
public class Networking {

    // =============  VARS

    public static Networking INSTANCE;

    // Netty settings.
    private String host;
    private int port;
    private String password;

    // Netty references.
    private EventLoopGroup eventLoopGroup;
    private ServerBootstrap serverBootstrap;
    private ChannelFuture channelFuture;
    private HashMap<String, Channel> channels = new HashMap<>();



    // =============  CONSTRUCTOR

    public Networking() {
        if (Networking.INSTANCE == null) { Networking.INSTANCE = this; }
        setHost(Main.INSTANCE.getConfig().getString("networking.host"));
        setPort(Main.INSTANCE.getConfig().getInt("networking.port"));
        setPassword(Main.INSTANCE.getConfig().getString("networking.password"));
    }



    // =============  BASIC LOGIC

    public void setup() {

        // Create an event loop & Server bootstrap.
        setEventLoopGroup(new NioEventLoopGroup());
        setServerBootstrap(new ServerBootstrap());

        // Configure the server.
        getServerBootstrap().group(getEventLoopGroup());
        getServerBootstrap().channel(NioServerSocketChannel.class);
        getServerBootstrap().localAddress(new InetSocketAddress(getHost(), getPort()));

        // Setup handler.
        getServerBootstrap().childHandler(new ChannelInitializer<SocketChannel>() {
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                Main.INSTANCE.log("[NET] Client connected from " + socketChannel.remoteAddress().getHostName() + ":" + Integer.toString(socketChannel.remoteAddress().getPort()));
                socketChannel.pipeline().addLast(new DataHandler());
            }
        });

    }


    /*
     * Sends a message to one channel.
     */
    public void sendToChannel(String channelName, String channel, String message) {
        try {
            String data = Crypto.encrypt(channel + "#" + message);
            getChannels().get(channelName).write(Unpooled.copiedBuffer(data, CharsetUtil.UTF_8));
            getChannels().get(channelName).flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
     * Sends a message to all channels.
     */
    public void sendToAll(String channel, String message) {
        try {
            String data = Crypto.encrypt(channel + "#" + message);
            for (Channel cnl : getChannels().values()) {
                cnl.write(Unpooled.copiedBuffer(data, CharsetUtil.UTF_8));
                cnl.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Sends a message to the sender.
     */
    public void sendToSender(ChannelHandlerContext ctx, String channel, String message) {
        try {
            String data = Crypto.encrypt(channel + "#" + message);
            ctx.channel().write(Unpooled.copiedBuffer(data, CharsetUtil.UTF_8));
            ctx.channel().flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    // =============  CONTROL LOGIC

    /*
     * Starts the server.
     */
    public void start() {

        try {
            setChannelFuture(getServerBootstrap().bind().sync());
            //getChannelFuture().channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Main.INSTANCE.log("[NET] Server started on " + getHost() + ":" + Integer.toString(getPort()));

    }


    /*
     * Shut down the server.
     */
    public void shutdown() {
        try {
            getEventLoopGroup().shutdownGracefully().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
