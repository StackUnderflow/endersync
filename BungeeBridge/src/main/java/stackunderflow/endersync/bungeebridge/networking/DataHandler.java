package stackunderflow.endersync.bungeebridge.networking;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import net.md_5.bungee.api.config.ServerInfo;
import stackunderflow.endersync.bungeebridge.Main;
import stackunderflow.endersync.bungeebridge.utils.Crypto;

import javax.crypto.BadPaddingException;
import java.util.UUID;


public class DataHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        // Prepare data.
        ByteBuf inBuffer = (ByteBuf) msg;
        String data = inBuffer.toString(CharsetUtil.UTF_8);
        String decryptedData = Crypto.decrypt(data);

        // Debug.
        Main.INSTANCE.logDebug("[NET] Recv: "+decryptedData);

        // Handle the message.
        handleMessage(ctx, decryptedData);

    }


    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        //ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {

        // CRYPTO
        if (cause.getMessage().contains("crypt")) {
            if (cause instanceof BadPaddingException) {
                Main.INSTANCE.logError("Crypto error! Please check that the key you are using is correct & the same on all servers!");
                return;
            }
        }

        cause.printStackTrace();
        //ctx.close();
    }



    /*
     * Handles a message.
     */
    private void handleMessage(ChannelHandlerContext ctx, String message) {

        // RET: Message invalid (Missing §).
        if (!message.contains("#")) { Main.INSTANCE.logError("[NET] Received invalid message: "+message); return; }

        // Prepare message.
        final String channel = message.split("#")[0];
        final String sender = message.split("#")[1];
        final String data = message.split("#")[2];


        // IDENT
        if (channel.equalsIgnoreCase("IDENT")) {

            // RET: Identifier already exists.
            if (Networking.INSTANCE.getChannels().containsKey(data)) {
                Main.INSTANCE.logWarning("A server tried to identify itself as " + data + " but another server already has the same name!");
                Main.INSTANCE.logWarning("Please check your configuration files.");
                return;
            }

            // Debug.
            Main.INSTANCE.logDebug("[NET] IDENT complete for " + data);

            // TODO: Add double name check and error

            // Save the requested identity.
            Networking.INSTANCE.getChannels().put(data, ctx.channel());

        }


        // RET: NO AUTHENTICATION.
        if (!Networking.INSTANCE.getChannels().containsKey(sender)) return;


        // CLOSE
        if (channel.equalsIgnoreCase("CLOSE")) {

            // Debug.
            Main.INSTANCE.logDebug("[NET] CLOSE channel for " + data);

            // Save the requested identity.
            Networking.INSTANCE.getChannels().remove(data);

        }


        // OP (Occupy Player)
        if (channel.equalsIgnoreCase("OP")) {
            UUID playerUUID = UUID.fromString(data);
            Main.INSTANCE.logDebug("Occupying player: "+playerUUID);
            if (!Main.INSTANCE.getPlayersOccupied().contains(playerUUID))
                Main.INSTANCE.getPlayersOccupied().add(playerUUID);
        }


        // UOP (Un-Occupy Player)
        if (channel.equalsIgnoreCase("UOP")) {
            UUID playerUUID = UUID.fromString(data);
            Main.INSTANCE.logDebug("Un-Occupying player: "+playerUUID);
            Main.INSTANCE.getPlayersOccupied().remove(playerUUID);
        }


        // IOP (Is-Occupied Player)
        if (channel.equalsIgnoreCase("IOP")) {
            UUID playerUUID = UUID.fromString(data);
            Main.INSTANCE.logDebug("Is-Occupied player: "+playerUUID);
            if (Main.INSTANCE.getPlayersOccupied().contains(playerUUID))
                Networking.INSTANCE.sendToSender(ctx, "IOP", playerUUID.toString()+";true");
            else
                Networking.INSTANCE.sendToSender(ctx, "IOP", playerUUID.toString()+";false");
        }


        // SYP (Sync Player - Sync is executed on the server the player currently is on)
        if (channel.equalsIgnoreCase("SYP")) {
            UUID playerUUID = UUID.fromString(data.split("%")[0]);
            ServerInfo server = Main.INSTANCE.getProxy().getPlayer(playerUUID).getServer().getInfo();
            Main.INSTANCE.logDebug("Sending sync request to server " + server.getName() + " for player: "+playerUUID);
            Networking.INSTANCE.sendToChannel(server.getName(), "SYP", data);
        }


        // SAP (Save Player - Save is executed on the server the player currently is on)
        if (channel.equalsIgnoreCase("SAP")) {
            UUID playerUUID = UUID.fromString(data.split("%")[0]);
            ServerInfo server = Main.INSTANCE.getProxy().getPlayer(playerUUID).getServer().getInfo();
            Main.INSTANCE.logDebug("Sending save request to server " + server.getName() + " for player: "+playerUUID);
            Networking.INSTANCE.sendToChannel(server.getName(), "SAP", data);
        }


    }

}
