package stackunderflow.endersync.serializers;

import org.bukkit.inventory.ItemStack;


/*
 * The base class.
 */
public interface ItemStackSerializer {


    /*
     * Logic.
     */
    public String toBase64(ItemStack itemStack);
    public ItemStack fromBase64(String data);


}
