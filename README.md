# EnderSync

# IMPORTANT:

This git is out of date and will be removed in the future!
This is because the project was moved and re-structured. 
**Please use [https://gitlab.com/endersuite/endersync](https://gitlab.com/endersuite/endersync) in the future**

### Keep your data up to sync.

This is the SourceCode for the EnderSync plugin by StackUnderflow.
It is open sourced under the Apache License 2.0 (See LICENSE file).
If you want to use the code, feel free to do so, as long as you obey the license terms.

**More details can be found here:**
[https://www.spigotmc.org/resources/endersync-sync-playerdata-between-servers-inventory-health-xp.64344/](https://www.spigotmc.org/resources/endersync-sync-playerdata-between-servers-inventory-health-xp.64344/)

## API

If you want to use the API, please take a look at the [Start page](https://gitlab.com/StackUnderflow/endersync/wikis/Start)
