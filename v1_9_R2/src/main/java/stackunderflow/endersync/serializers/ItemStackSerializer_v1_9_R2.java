package stackunderflow.endersync.serializers;

import net.minecraft.server.v1_9_R2.ItemStack;
import net.minecraft.server.v1_9_R2.NBTCompressedStreamTools;
import net.minecraft.server.v1_9_R2.NBTTagCompound;
import net.minecraft.server.v1_9_R2.NBTTagList;
import org.bukkit.craftbukkit.v1_9_R2.inventory.CraftItemStack;

import java.io.*;
import java.math.BigInteger;

/*
 * Version 1.9 implementation.
 */
public class ItemStackSerializer_v1_9_R2 implements ItemStackSerializer {

    /*
     * ItemStack
     */
    public String toBase64(org.bukkit.inventory.ItemStack item) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutput = new DataOutputStream(outputStream);

        NBTTagList nbtTagListItems = new NBTTagList();
        NBTTagCompound nbtTagCompoundItem = new NBTTagCompound();

        ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        nmsItem.save(nbtTagCompoundItem);

        nbtTagListItems.add(nbtTagCompoundItem);

        try {
            NBTCompressedStreamTools.a(nbtTagCompoundItem, (DataOutput) dataOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new BigInteger(1, outputStream.toByteArray()).toString(32);
    }
    public org.bukkit.inventory.ItemStack fromBase64(String data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(new BigInteger(data, 32).toByteArray());

        NBTTagCompound nbtTagCompoundRoot = null;
        try {
            nbtTagCompoundRoot = NBTCompressedStreamTools.a(new DataInputStream(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ItemStack nmsItem = ItemStack.createStack(nbtTagCompoundRoot);
        org.bukkit.inventory.ItemStack item = (org.bukkit.inventory.ItemStack) CraftItemStack.asBukkitCopy(nmsItem);

        return item;
    }

}
