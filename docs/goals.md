This file includes goals / features which I want to implement / support in the near future.

- Different backend typed: MySQL, MongoDB, maybe some others as well
- Custom modules which can be crated by other plugins.
- Custom modules can upload their own meta data.
- Implement automatic SQL escaping into the execution functions
- Add functionality for none uuid based data management
- Add / Move to Async calls
- Add /sync rate limit option



v3.3.3 | FIX: Sync issue when switching servers | CHANGE: Permissions

[B][B]FIX | Sync issue when switching servers[/B][/B]
Somebody pointed out, that there is an issue, where data would not be synced when switching servers, because the sync on the new server would already be started when the save on the old server was not even finished.
This is now fixed!
[B]
CHANGE | [I]Changed permissions[/I][/B]
I decided to change the permissions due to some feedback I got, so that it is quite clear which commands are intended for admins only (this is also to make later commands added, sorted in simple categories).

[B]- | [I]Removed "version" node from config.yml[/I][/B]
It was useless anyways.

[B]FIX | [I]No permission message (german translation)[/I][/B]
Fixed a simple character missing.