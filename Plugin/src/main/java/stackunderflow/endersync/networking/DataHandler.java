package stackunderflow.endersync.networking;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.Crypto;
import stackunderflow.endersync.utils.Messaging;
import stackunderflow.endersync.utils.Promise;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;


@Getter
@Setter
public class DataHandler extends SimpleChannelInboundHandler<ByteBuf> {


    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        Main.INSTANCE.log("[NET] Connected to server on " + Networking.INSTANCE.getHost() + ":" + Integer.toString(Networking.INSTANCE.getPort()));
        Networking.INSTANCE.getChannels().add(ctx.channel());
        sendIdent();
    }


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {

        // Prepare data.
        ByteBuf in = buf;
        String data = in.toString(CharsetUtil.UTF_8);
        String message = Crypto.decrypt(data);

        // Debug.
        Main.INSTANCE.logDebug("[NET] Recv: "+message);

        // Handle message.
        handleMessage(ctx, message);

    }


    @Override
    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable cause) {
        cause.printStackTrace();
        //channelHandlerContext.close();
    }



    /*
     * Handles a message.
     */
    private void handleMessage(ChannelHandlerContext ctx, String message) {

        // RET: Message invalid (Missing §).
        if (!message.contains("#")) { Main.INSTANCE.logError("[NET] Received invalid message: "+message); return; }

        // Prepare message.
        final String channel = message.split("#")[0];
        final String data = message.split("#")[1];


        // IOP (Is-Occupied Player)
        if (channel.equalsIgnoreCase("IOP")) {
            String[] splitData = data.split(";");
            UUID playerUUID = UUID.fromString(splitData[0]);
            String responseValue = splitData[1];

            if (Messaging.occupiedPlayersCallbacks.containsKey(playerUUID)) {
                CompletableFuture<Boolean> completableFuture = Messaging.occupiedPlayersCallbacks.get(playerUUID);
                Messaging.occupiedPlayersCallbacks.remove(playerUUID);
                if (responseValue.equalsIgnoreCase("true")) completableFuture.complete(true);
                else completableFuture.complete(false);
            }
        }


        // SYP
        if (channel.equalsIgnoreCase("SYP")) {

            // Prepare data.
            String[] splitData = data.split("%");
            UUID targetUUID = UUID.fromString(splitData[0]);
            String[] moduleNames = data.substring(splitData[0].length()+1, data.length()).split(";");

            // RET: Player not on server.
            Player targetPlayer = Bukkit.getPlayer(targetUUID);
            if (targetPlayer == null) return;

            // Get module list.
            List<PlayerSyncModule> modules = new ArrayList<>();
            for (String moduleName : moduleNames) {
                try {
                    modules.add((PlayerSyncModule) SyncManager.INSTANCE.getModule(moduleName));
                } catch (ModuleNotFoundException e) {
                    e.printStackTrace();
                }
            }

            // Sync player.
            SyncManager.INSTANCE.syncPlayer(targetPlayer, modules, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

        }


        // SAP
        if (channel.equalsIgnoreCase("SAP")) {

            // Prepare data.
            String[] splitData = data.split("%");
            UUID targetUUID = UUID.fromString(splitData[0]);
            String[] moduleNames = data.substring(splitData[0].length()+1, data.length()).split(";");

            // RET: Player not on server.
            Player targetPlayer = Bukkit.getPlayer(targetUUID);
            if (targetPlayer == null) return;

            // Get module list.
            List<PlayerSyncModule> modules = new ArrayList<>();
            for (String moduleName : moduleNames) {
                try {
                    modules.add((PlayerSyncModule) SyncManager.INSTANCE.getModule(moduleName));
                } catch (ModuleNotFoundException e) {
                    e.printStackTrace();
                }
            }

            // Sync player.
            SyncManager.INSTANCE.savePlayer(targetPlayer, modules, new Promise() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError(Exception ex) {}
            });

        }


    }



    /*
     * Sends identification data to BungeeBridge.
     */
    private void sendIdent() {
        Networking.INSTANCE.sendToBridge("IDENT", Networking.INSTANCE.getName());
    }

}
