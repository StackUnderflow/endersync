package stackunderflow.endersync.networking;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.CharsetUtil;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.utils.Configuration;
import stackunderflow.endersync.utils.Crypto;

import java.net.ConnectException;
import java.net.InetSocketAddress;


/*
 * Handles networking (sockets).
 * It is used to establish a connection to and use the BungeeBridge.
 */
@Getter
@Setter
public class Networking {

    // =============  VARS

    public static Networking INSTANCE;

    // Netty settings.
    private String host;
    private int port;
    private String password;
    private String name;

    // Netty references.
    private EventLoopGroup eventLoopGroup;
    private Bootstrap bootstrap;
    private ChannelGroup channels;



    // =============  CONSTRUCTOR

    public Networking() {
        if (Networking.INSTANCE == null) { Networking.INSTANCE = this; }

        setHost(Configuration.INSTANCE.get().getString("networking.bridgeHost"));
        setPort(Configuration.INSTANCE.get().getInt("networking.bridgePort"));
        setPassword(Configuration.INSTANCE.get().getString("networking.password"));
        setName(Configuration.INSTANCE.get().getString("networking.name"));

        // TODO Add password check for min length and if still default value

    }



    // =============  BASIC LOGIC

    public void setup() {

        // Create an event loop & Server bootstrap.
        setEventLoopGroup(new NioEventLoopGroup());
        setBootstrap(new Bootstrap());
        setChannels(new DefaultChannelGroup("main", GlobalEventExecutor.INSTANCE));

        // Configure the client.
        getBootstrap().group(getEventLoopGroup());
        getBootstrap().channel(NioSocketChannel.class);
        getBootstrap().remoteAddress(new InetSocketAddress(getHost(), getPort()));

        // Setup handler.
        getBootstrap().handler(new ChannelInitializer<SocketChannel>() {
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                socketChannel.pipeline().addLast(new DataHandler());
            }
        });

    }


    /*
     * Sends a message to the channel.
     */
    public void sendToBridge(String channel, String message) {
        try {
            String data = Crypto.encrypt("" + channel + "#" + getName() + "#" + message);
            getChannels().write(Unpooled.copiedBuffer(data, CharsetUtil.UTF_8));
            getChannels().flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    // =============  CONTROL LOGIC

    /*
     * Starts the server.
     */
    public void start() {

        try {
            ChannelFuture channelFuture = getBootstrap().connect();
            channelFuture.addListener(f -> {
                if (!f.isSuccess() && f.cause() instanceof ConnectException) {

                    Main.INSTANCE.logError("Could not connect to BungeeBridge on " +
                            Networking.INSTANCE.getHost() + ":" + Integer.toString(Networking.INSTANCE.getPort()) + " is it running?");
                    Bukkit.getPluginManager().disablePlugin(Main.INSTANCE);

                }
            });
            channelFuture.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    /*
     * Shut down the server.
     */
    public void shutdown() {
        sendToBridge("CLOSE", getName());
        try {
            getEventLoopGroup().shutdownGracefully().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
