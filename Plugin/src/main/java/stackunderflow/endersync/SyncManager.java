package stackunderflow.endersync;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.events.*;
import stackunderflow.endersync.exceptions.*;
import stackunderflow.endersync.modules.DataSyncModule;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.modules.SyncModule;
import stackunderflow.endersync.networking.Networking;
import stackunderflow.endersync.utils.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


/*
 * Manages al the sync & save functionality.
 * -> Starting / Stopping syncs ...
 */
@Getter
@Setter
public class SyncManager {

    // ================================     VARS

    // References
    public static SyncManager INSTANCE;


    // Settings
    private HashMap<String, PlayerSyncModule> activePlayerModules;
    private HashMap<String, DataSyncModule> activeDataModules;


    // State
    private boolean syncing = false;



    // ================================     CONSTRUCTOR

    public SyncManager() {
        if (SyncManager.INSTANCE == null) {
            SyncManager.INSTANCE = this;
        }
        setActivePlayerModules(new HashMap<>());
        setActiveDataModules(new HashMap<>());
    }



    // ================================     Module management LOGIC


    /*
     * Enables the provided module.
     */
    public void enableModule(SyncModule module) throws ModuleExceptions.ModuleAlreadyEnabledException, ModuleExceptions.InvalidModuleException {

        if (module instanceof PlayerSyncModule) {

            // THROW: Already enabled.
            if (getActivePlayerModules().containsKey(module.getName())) {
                throw new ModuleExceptions.ModuleAlreadyEnabledException("Module '"+module.getName()+"' is already enabled / registered!", module);
            }

            // THROW: Invalid module.
            if (!module.isValid()) {
                throw new ModuleExceptions.InvalidModuleException("Module '"+module.getName()+"' is invalid and won't be enabled!", module);
            }

            Main.INSTANCE.logDebug("  [+ MOD] "+module.getName());

            getActivePlayerModules().put(module.getName(), (PlayerSyncModule) module);

        }

        if (module instanceof DataSyncModule) {

            // THROW: Already enabled.
            if (getActiveDataModules().containsKey(module.getName())) {
                throw new ModuleExceptions.ModuleAlreadyEnabledException("Module '"+module.getName()+"' is already enabled / registered!", module);
            }

            // THROW: Invalid module.
            if (!module.isValid()) {
                throw new ModuleExceptions.InvalidModuleException("Module '"+module.getName()+"' is invalid and won't be enabled!", module);
            }

            Main.INSTANCE.logDebug("  [+ MOD] "+module.getName());

            getActiveDataModules().put(module.getName(), (DataSyncModule) module);

        }

    }

    /*
     * Disables a module if it is enabled.
     */
    public void disableModule(String name) throws ModuleNotFoundException {

        // THROW: Not found.
        if (!getActivePlayerModules().containsKey(name)) {
            if (!getActiveDataModules().containsKey(name)) {
                throw new ModuleNotFoundException("Module '"+name+"' could not be found!");
            }
            else {
                getActiveDataModules().remove(name);
            }
        }
        else {
            getActivePlayerModules().remove(name);
        }

        Main.INSTANCE.logDebug("  [- MOD] "+name);

    }

    /*
     * Gets a module.
     */
    public SyncModule getModule(String name) throws ModuleNotFoundException {

        // THROW: Not found.
        if (!getActivePlayerModules().containsKey(name)) {
            if (!getActiveDataModules().containsKey(name)) {
                throw new ModuleNotFoundException("Module '"+name+"' could not be found!");
            }
            else {
                return getActiveDataModules().get(name);
            }
        }
        else {
            return getActivePlayerModules().get(name);
        }

    }

    /*
     * Returns whether a module is enabled.
     */
    public boolean isModuleEnabled(String name) {

        if (!getActivePlayerModules().containsKey(name)) {
            if (!getActiveDataModules().containsKey(name)) {
                return false;
            }
        }
        return true;

    }

    /*
     * Unblocks the player from all modules.
     */
    public void unblockPlayerFromAllModules(Player player) {
        for (PlayerSyncModule module : getActivePlayerModules().values()) module.unblockPlayer(player);
    }



    // ================================     UTIL LOGIC

    /*
     * Returns whether a player is occupied.
     * True means, that BungeeCord lists him as occupied.
     * When a player is occupied, any sync / save action could lead to bugs / glitches!
     */
    public CompletableFuture<Boolean> isPlayerOccupied(Player player) { return Messaging.isPlayerOccupied(player); }
    public CompletableFuture<Boolean> isPlayerOccupied(UUID uuid) { return Messaging.isPlayerOccupied(uuid); }


    /*
     * Updated the occupied status on the Bungee server.
     */
    public void occupyPlayer(UUID playerUUID) {
        Networking.INSTANCE.sendToBridge( "OP", playerUUID.toString());
    }
    public void unOccupyPlayer(UUID playerUUID) {
        Networking.INSTANCE.sendToBridge( "UOP", playerUUID.toString());
    }


    /*
     * Returns whether the module is blocked.
     */
    public boolean isPlayerModuleBlocked(Player player, String moduleName) {
        PlayerSyncModule module = null;
        try {
            module = (PlayerSyncModule) SyncManager.INSTANCE.getModule(moduleName);
        }
        catch (ModuleNotFoundException e) {
            return false;
        }
        return module.isPlayerBlocked(player);
    }



    // ================================     Sync / Save LOGIC


    /*
     * Syncs a players modules.
     */
    public void syncPlayer(Player player, Promise callback) {
        List<PlayerSyncModule> modules = new ArrayList<>();
        for (PlayerSyncModule module : getActivePlayerModules().values()) modules.add(module);
        syncPlayer(player, modules, callback);
    }
    public void syncPlayer(Player player, PlayerSyncModule module, Promise callback) {
        List<PlayerSyncModule> modules = new ArrayList<>();
        modules.add(module);
        syncPlayer(player, modules, callback);
    }
    public void syncPlayer(Player player, List<PlayerSyncModule> modules, Promise callback) {

        boolean occupied;
        try {
            occupied = isPlayerOccupied(player).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return;
        }

        // RET: Player is occupied.
        if (occupied) callback.onError(new PlayerOccupiedException("Player is occupied!", player));

        // Call PlayerSyncStartEvent.
        if (!BukkitUtils.call(new PlayerSyncStartEvent(player))) { callback.onError(null); return; };

        // Update Bungee.
        occupyPlayer(player.getUniqueId());

        // Info.
        Main.INSTANCE.logDebug("Syncing player: "+player.getUniqueId().toString()+" / "+player.getName());
        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("sync.syncStarted")).enableProgress().sendMessageTo(player);
        if (Configuration.INSTANCE.get("features").getBoolean("features.sounds.enabled")) {
            player.playSound(player.getLocation(), Sounds.ORB_PICKUP.bukkitSound(), 4f, 4f);
        }

        // Fetch rows.
        HashMap<PlayerSyncModule, Row> rows = new HashMap<>();
        for (PlayerSyncModule module : modules) {
            Row row = null;
            try {
                row = module.getPlayerRow(player);
            } catch (SQLException | RowNotFoundException e) {
                e.printStackTrace();

                // Call PlayerSyncFailedEvent.
                BukkitUtils.call(new PlayerSyncFailedEvent(player));

                callback.onError(null);
                return;
            }

            rows.put(module, row);
        }

        // Sync the modules.
        for (PlayerSyncModule module : modules) {

            // Block the player.
            module.blockPlayer(player);

            Row row = rows.get(module);

            // Call SyncModuleStartedPlayerSyncEvent.
            if (!BukkitUtils.call(new SyncModuleStartedPlayerSync(player, module))) continue;

            // Sync the player and unblock him.
            try {
                module.onPlayerSync(row, player);
            }
            catch (Exception e) {

                // Unblock player from all modules.
                // TODO: Unoccupy
                unblockPlayerFromAllModules(player);

                // Info.
                Main.INSTANCE.logError("Module threw exception during sync: ");
                e.printStackTrace();

                // Call PlayerSyncFailedEvent.
                BukkitUtils.call(new PlayerSyncFailedEvent(player));

                // Info.
                new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("sync.syncFailed")).setSuccess(false).sendMessageTo(player);
                if (Main.INSTANCE.getConfig().getBoolean("plugin.features.sounds.enabled"))
                    player.playSound(player.getLocation(), Sounds.ITEM_BREAK.bukkitSound(), 4f, 4f);

                // Kick.
                if (Configuration.INSTANCE.get("features").getBoolean("features.kickOnFailedSync.enabled"))
                    player.kickPlayer(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("sync.syncFailed")).getSTR());


                callback.onError(null);
                return;

            }

            module.unblockPlayer(player);

            // Call SyncModuleFinishedPlayerSyncEvent.
            BukkitUtils.call(new SyncModuleFinishedPlayerSync(player, module));

        }

        // Update Bungee.
        unOccupyPlayer(player.getUniqueId());

        // Info.
        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("sync.syncCompleted")).setSuccess(true).sendMessageTo(player);
        if (Configuration.INSTANCE.get("features").getBoolean("features.sounds.enabled"))
            player.playSound(player.getLocation(), Sounds.LEVEL_UP.bukkitSound(), 4f, 4f);

        // Call PlayerSyncFinishedEvent.
        BukkitUtils.call(new PlayerSyncFinishedEvent(player));

        callback.onSuccess();

    }

    /*
     * Syncs a player by UUID.
     * This enables syncing him over BungeeBridge.
     */
    public void syncPlayer(UUID playerUUID, PlayerSyncModule module, Promise callback) {
        List<PlayerSyncModule> modules = new ArrayList<>();
        modules.add(module);
        syncPlayer(playerUUID, modules, callback);
    }
    public void syncPlayer(UUID playerUUID, List<PlayerSyncModule> modules, Promise callback) {

        // Check of the player is on current server.
        if (Bukkit.getPlayer(playerUUID) != null) syncPlayer(Bukkit.getPlayer(playerUUID), modules, callback);

        // Build message.
        String message = playerUUID.toString() + "%";
        for (PlayerSyncModule module : modules) message = message + module.getName() + ";";
        message = message.substring(0, message.length()-1);

        // Send request to BungeeBridge.
        Networking.INSTANCE.sendToBridge("SYP", message);

    }



    /*
     * Saves a players modules.
     */
    public void savePlayer(Player player, Promise callback) {
        List<PlayerSyncModule> modules = new ArrayList<>();
        for (PlayerSyncModule module : getActivePlayerModules().values()) modules.add(module);
        savePlayer(player, modules, callback);
    }
    public void savePlayer(Player player, PlayerSyncModule module, Promise callback) {
        List<PlayerSyncModule> modules = new ArrayList<>();
        modules.add(module);
        savePlayer(player, modules, callback);
    }
    public void savePlayer(Player player, List<PlayerSyncModule> modules, Promise callback) {

        boolean occupied;
        try {
            occupied = isPlayerOccupied(player).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return;
        }

        // RET: Player is occupied.
        if (occupied) callback.onError(new PlayerOccupiedException("Player is occupied!", player));

        // Call PlayerSaveStartEvent.
        if (!BukkitUtils.call(new PlayerSaveStartEvent(player))) { callback.onError(null); return; }


        // Update Bungee.
        occupyPlayer(player.getUniqueId());


        // Info.
        Main.INSTANCE.logDebug("Saving player: "+player.getUniqueId().toString()+" / "+player.getName());
        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("save.saveStarted")).enableProgress().sendMessageTo(player);
        if (Configuration.INSTANCE.get("features").getBoolean("features.sounds.enabled")) {
            player.playSound(player.getLocation(), Sounds.ORB_PICKUP.bukkitSound(), 4f, 4f);
        }

        // Fetch rows.
        HashMap<PlayerSyncModule, Row> rows = new HashMap<>();
        for (PlayerSyncModule module : modules) {
            Row row = null;
            try {
                row = module.getPlayerRow(player);
            } catch (SQLException | RowNotFoundException e) {
                e.printStackTrace();

                // Call PlayerSaveFailedEvent.
                BukkitUtils.call(new PlayerSaveFailedEvent(player));


                callback.onError(null);
                return;
            }

            rows.put(module, row);
        }

        // Sync the modules.
        for (PlayerSyncModule module : modules) {

            // Block the player.
            module.blockPlayer(player);

            Row row = rows.get(module);

            // Call SyncModuleStartedPlayerSyncEvent.
            if (!BukkitUtils.call(new SyncModuleStartedPlayerSync(player, module))) continue;

            // Sync the player and unblock him.
            try {
                module.onPlayerSave(row, player);
            }
            catch (Exception e) {

                // Unblock player from all modules.
                unblockPlayerFromAllModules(player);

                // Info.
                Main.INSTANCE.logError("Module threw exception during save: ");
                e.printStackTrace();

                // Call PlayerSaveFailedEvent.
                BukkitUtils.call(new PlayerSaveFailedEvent(player));

                // Info.
                new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("save.saveFailed")).setSuccess(false).sendMessageTo(player);
                if (Main.INSTANCE.getConfig().getBoolean("plugin.features.sounds.enabled"))
                    player.playSound(player.getLocation(), Sounds.ITEM_BREAK.bukkitSound(), 4f, 4f);

                callback.onError(null);
                return;

            }

            module.unblockPlayer(player);

            // Call SyncModuleFinishedPlayerSave.
            BukkitUtils.call(new SyncModuleFinishedPlayerSave(player, module));

        }

        // Update Bungee.
        unOccupyPlayer(player.getUniqueId());

        // Info.
        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("save.saveCompleted")).setSuccess(true).sendMessageTo(player);
        if (Configuration.INSTANCE.get("features").getBoolean("features.sounds.enabled"))
            player.playSound(player.getLocation(), Sounds.LEVEL_UP.bukkitSound(), 4f, 4f);

        // Call PlayerSaveFinishedEvent.
        BukkitUtils.call(new PlayerSaveFinishedEvent(player));

        callback.onSuccess();

    }

    /*
     * Saves a player by UUID.
     * This enables saving him over BungeeBridge.
     */
    public void savePlayer(UUID playerUUID, PlayerSyncModule module, Promise callback) {
        List<PlayerSyncModule> modules = new ArrayList<>();
        modules.add(module);
        savePlayer(playerUUID, modules, callback);
    }
    public void savePlayer(UUID playerUUID, List<PlayerSyncModule> modules, Promise callback) {

        // Check of the player is on current server.
        if (Bukkit.getPlayer(playerUUID) != null) savePlayer(Bukkit.getPlayer(playerUUID), modules, callback);

        // Build message.
        String message = playerUUID.toString() + "%";
        for (PlayerSyncModule module : modules) message = message + module.getName() + ";";
        message = message.substring(0, message.length()-1);

        // Send request to BungeeBridge.
        Networking.INSTANCE.sendToBridge("SAP", message);

    }

}
