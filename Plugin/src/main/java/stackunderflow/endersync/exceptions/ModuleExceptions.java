package stackunderflow.endersync.exceptions;

import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersync.modules.SyncModule;

/*
 * A container class for all module related exceptions.
 */
public class ModuleExceptions {

    // ModuleAlreadyEnabledException
    @Getter
    @Setter
    public static class ModuleAlreadyEnabledException extends BaseException {
        private SyncModule module;
        public ModuleAlreadyEnabledException(String message, SyncModule module) {
            super(message);
            setModule(module);
        }
    }

    // InvalidModuleException
    @Getter
    @Setter
    public static class InvalidModuleException extends BaseException {
        private SyncModule module;
        public InvalidModuleException(String message, SyncModule module) {
            super(message);
            setModule(module);
        }
    }

}
