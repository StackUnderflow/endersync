package stackunderflow.endersync.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;


/*
 * Thrown when an action requires at least 1 player to be online.
 */
@Getter
@Setter
public class NonePlayersOnline extends BaseException {
    public NonePlayersOnline(String message) {
        super(message);
    }
}
