package stackunderflow.endersync.listeners.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.events.SyncModuleStartedPlayerSave;
import stackunderflow.endersync.utils.Configuration;

import java.util.List;

/*
 * Contains the logic for enabling the whitelist feature for saving the inventory.
 */
public class InventorySaveWhitelistHandler implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEvent(SyncModuleStartedPlayerSave event) {

        Main.INSTANCE.logError("EVENT CALED!!");

        // RET: Adaptive mode not enabled.
        System.out.println(Configuration.INSTANCE.get("features").getString("features.save.inventory.adaptiveMode"));
        if (!Configuration.INSTANCE.get("features").getString("features.save.inventory.adaptiveMode").equalsIgnoreCase("WHITELIST")) return;

        // Get items on whitelist.
        List<String> whitelistItems = Configuration.INSTANCE.get("features").getStringList("features.save.inventory.whitelist");

        System.out.println(whitelistItems);

    }

}
