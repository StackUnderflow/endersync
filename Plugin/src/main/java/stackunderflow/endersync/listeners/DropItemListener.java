package stackunderflow.endersync.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;

import java.util.concurrent.ExecutionException;


/*
 * Handles players dropping items.
 * -> Cancel them if blocked.
 */
public class DropItemListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemDrop(PlayerDropItemEvent event) {

        Player player = event.getPlayer();

        if (Main.INSTANCE.getLockedPlayers().contains(player.getUniqueId())) event.setCancelled(true);

    }

}
