package stackunderflow.endersync.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.utils.Promise;


/*
 * Handles players leaving.
 * -> Starting the save process.
 */
public class PlayerQuitListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {

        final Player player = event.getPlayer();

        // Start save.
        SyncManager.INSTANCE.savePlayer(player, new Promise() {
            @Override
            public void onSuccess(){}
            @Override
            public void onError(Exception ex) {
                System.out.println("SAVE RROR!");
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        });

    }

}
