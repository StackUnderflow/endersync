package stackunderflow.endersync.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.commands.manipulation.CommandEnd;
import stackunderflow.endersync.commands.manipulation.CommandInv;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.NonePlayersOnline;
import stackunderflow.endersync.modules.EnderChestPlayerSyncModule;
import stackunderflow.endersync.modules.InventoryPlayerSyncModule;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.Promise;

import java.util.UUID;


/*
 * Handles inventory closes.
 * -> Updates when using invsee.
 */
public class InventoryCloseListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClose(InventoryCloseEvent event) {

        Player player = (Player) event.getPlayer();
        Inventory inventory = event.getInventory();

        // Check if inventory is an enderchest, if so, save the inv.
        if (inventory.getType().equals(InventoryType.ENDER_CHEST) && SyncManager.INSTANCE.isModuleEnabled("enderchest") && !CommandEnd.activeInvseePlayer.keySet().contains(player.getName())) {

            // Get the module.
            PlayerSyncModule module = null;
            try {
                module = (PlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest");
            }
            catch (ModuleNotFoundException e) {
                // TODO: Add module not enabled player message..
                return;
            }

            SyncManager.INSTANCE.savePlayer(player, module, new Promise() {
                @Override
                public void onSuccess() {
                    // Nothing
                }


                @Override
                public void onError(Exception ex) {
                    // Display error
                }
            });

        }

        // ENDERCHEST INVSEE
        if (inventory.getType().equals(InventoryType.ENDER_CHEST) && SyncManager.INSTANCE.isModuleEnabled("enderchest") && CommandEnd.activeInvseePlayer.keySet().contains(player.getName())) {

            // Get the module.
            EnderChestPlayerSyncModule tmpModule = null;
            try {
                tmpModule = (EnderChestPlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest");
            }
            catch (ModuleNotFoundException e) {
                // TODO: Add module not enabled player message..
                return;
            }
            final EnderChestPlayerSyncModule module = tmpModule;

            // Cache the target player UUID & Remove from list.
            UUID targetPlayer = CommandEnd.activeInvseePlayer.get(player.getUniqueId());
            CommandEnd.activeInvseePlayer.remove(player.getUniqueId());

            // Update enderchest data.
            boolean res = module.setPlayerEnderChestInventory(targetPlayer, inventory);

            // TODO: ADD FALSE CHECK

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                // Sync the target player.
                SyncManager.INSTANCE.syncPlayer(targetPlayer, module, new Promise() {
                    @Override
                    public void onSuccess() {

                    }


                    @Override
                    public void onError(Exception ex) {

                    }
                });

            }, 20L);

        }





        // INVENTORY INVSEE
        if (CommandInv.activeInvseePlayer.keySet().contains(player.getUniqueId()) && SyncManager.INSTANCE.isModuleEnabled("inventory")) {

            // Get the module.
            InventoryPlayerSyncModule tmpModule = null;
            try {
                tmpModule = (InventoryPlayerSyncModule) SyncManager.INSTANCE.getModule("inventory");
            }
            catch (ModuleNotFoundException e) {
                // TODO: Add module not enabled player message..
                return;
            }
            final InventoryPlayerSyncModule module = tmpModule;

            // Cache the target player UUID & Remove from list.
            UUID targetPlayer = CommandEnd.activeInvseePlayer.get(player.getUniqueId());
            CommandEnd.activeInvseePlayer.remove(player.getUniqueId());

            // Update enderchest data.
            boolean res = module.setPlayerInventory(targetPlayer, inventory);

            // TODO ADD FALSE CHECK

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                // Sync the target player.
                SyncManager.INSTANCE.syncPlayer(targetPlayer, module, new Promise() {
                    @Override
                    public void onSuccess() {}


                    @Override
                    public void onError(Exception ex) {}
                });

            }, 20L);

        }


    }

}

