package stackunderflow.endersync.api;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.ModuleExceptions;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.RowNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;

import java.sql.SQLException;


/*
 * The API which can be accessed by all plugins.
 * It can be used to get playerData
 */
@Getter
@Setter
public class EnderSyncAPI {

    // ================================     VARS

    // References




    // Settings




    // ================================     CONSTRUCTOR

    public EnderSyncAPI() {}



    // ================================     LOGIC


    /*
     * Returns the row for a module for a player.
     */
    public Row getPlayerDataRow(OfflinePlayer player, PlayerSyncModule module) throws SQLException, RowNotFoundException {

        // Get the data row.
        return module.getTableManager().getPlayerRow(player);

    }


    // ================================     MODULES


    /*
     * Registers a module.
     * This should be called for every module inside of "onEnable"
     */
    public void registerModule(PlayerSyncModule module) throws ModuleExceptions.ModuleAlreadyEnabledException, ModuleExceptions.InvalidModuleException {
        SyncManager.INSTANCE.enableModule(module);
    }


    /*
     * Get's a module if it is registered.
     */
    public PlayerSyncModule getModule(String name) throws ModuleNotFoundException {
        return (PlayerSyncModule) SyncManager.INSTANCE.getModule(name);
    }


    /*
     * Returns whether a module is enabled or not.
     */
    public boolean isModuleEnabled(String name) {
        return SyncManager.INSTANCE.isModuleEnabled(name);
    }


    // ================================     SYNC & SAVE


    /*
     * Sync's the player.
     */
    /*public void syncPlayer(Player player) {
        syncPlayer(player, false);
    }
    public void syncPlayer(Player player, boolean silent) {
        SyncManager.INSTANCE.startPlayerSync(player, silent);
    }
    public void syncPlayer(Player player, PlayerSyncModule module) { syncPlayer(player, false, module); }
    public void syncPlayer(Player player, boolean silent, PlayerSyncModule module) { SyncManager.INSTANCE.startPlayerSync(player, silent, module); }*/


    /*
     * Saves the player.
     */
    /*public void savePlayer(Player player) {
        savePlayer(player, false);
    }
    public void savePlayer(Player player, boolean silent) {
        SyncManager.INSTANCE.startPlayerSave(player, silent);
    }
    public void savePlayer(Player player, PlayerSyncModule module) { savePlayer(player, false, module); }
    public void savePlayer(Player player, boolean silent, PlayerSyncModule module) { SyncManager.INSTANCE.startPlayerSave(player, silent, module); }*/


    /*
     * Sync's data of a module.
     */
    //public void startDataSync(PlayerSyncModule module, Object meta) { SyncManager.INSTANCE.startDataSync(module, meta); }


    /*
     * Saves data of a module.
     */
    //public void startDataSave(PlayerSyncModule module, Object meta) { SyncManager.INSTANCE.startDataSave(module, meta); }



    // ================================     UTILS


    /*
     * Returns whether a player is blocked by a module.
     * This can be used to block events & actions when save / sync is in progress.
     */
    public boolean isPlayerBlockedByModule(Player player, String moduleName) {
        try {
            return ((PlayerSyncModule) SyncManager.INSTANCE.getModule(moduleName)).isPlayerBlocked(player);
        }
        catch (ModuleNotFoundException e) {
            return false;
        }
    }


    /*
     * Return the lastSeen value for a player.
     */
    public long getPlayerLastSeen() {
        return 0;
    }

}
