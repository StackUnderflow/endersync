package stackunderflow.endersync.utils;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

/*
 * Provides sound utils, for all MC versions.
 */
public class SoundUtil {

    public static Sound getSound(String soundName) {
        boolean legacy = (Bukkit.getVersion().contains("1.7") || Bukkit.getVersion().contains("1.8"));
        if (soundName.equals("ENTITY_EXPERIENCE_ORB_PICKUP") && legacy) return Sound.valueOf("ORB_PICKUP");
        if (soundName.equals("ENTITY_PLAYER_LEVELUP") && legacy) return Sound.valueOf("LEVEL_UP");
        if (soundName.contains("ENTITY_ITEM_BREAK") && legacy) return Sound.valueOf("ITEM_BREAK");
        return Sound.valueOf(soundName);
    }

}
