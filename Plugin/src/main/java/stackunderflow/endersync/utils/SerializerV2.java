package stackunderflow.endersync.utils;


/*import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;*/
import org.bukkit.Bukkit;
import org.bukkit.Material;
//import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/*
 * This class contains serialization logic.
 */
public class SerializerV2 {

    /*
     * ItemStack
     */
    /*public static String itemStackToBase64(org.bukkit.inventory.ItemStack item) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutput = new DataOutputStream(outputStream);

        NBTTagList nbtTagListItems = new NBTTagList();
        NBTTagCompound nbtTagCompoundItem = new NBTTagCompound();

        ItemStack nmsItem = CraftItemStack.asNMSCopy(item);

        nmsItem.save(nbtTagCompoundItem);

        nbtTagListItems.add(nbtTagCompoundItem);

        try {
            NBTCompressedStreamTools.a(nbtTagCompoundItem, (DataOutput) dataOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new BigInteger(1, outputStream.toByteArray()).toString(32);
    }
    public static org.bukkit.inventory.ItemStack itemStackFromBase64(String data) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(new BigInteger(data, 32).toByteArray());

        NBTTagCompound nbtTagCompoundRoot = null;
        try {
            nbtTagCompoundRoot = NBTCompressedStreamTools.a(new DataInputStream(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }

        net.minecraft.server.v1_12_R1.ItemStack nmsItem = new net.minecraft.server.v1_12_R1.ItemStack(nbtTagCompoundRoot);
        org.bukkit.inventory.ItemStack item = (org.bukkit.inventory.ItemStack) CraftItemStack.asBukkitCopy(nmsItem);

        return item;
    }
    public static String itemStacksToBase64(org.bukkit.inventory.ItemStack[] items) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BukkitObjectOutputStream dataOutput;
        try {
            dataOutput = new BukkitObjectOutputStream(outputStream);

            // Content Size
            // Contents
            dataOutput.writeInt(items.length);

            int index = 0;
            for (org.bukkit.inventory.ItemStack is : items) {
                if (is != null && is.getType() != Material.AIR) {
                    dataOutput.writeObject(itemStackToBase64(is));
                } else {
                    dataOutput.writeObject(null);
                }
                dataOutput.writeInt(index);
                index++;
            }
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        }
        catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }
    public static org.bukkit.inventory.ItemStack[] itemStacksFromBase64(String items) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(items));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

            int size = dataInput.readInt();

            org.bukkit.inventory.ItemStack[] list = new org.bukkit.inventory.ItemStack[size];
            // Read the serialized inventory
            for (int i = 0; i < size; i++) {
                Object utf = dataInput.readObject();
                int slot = dataInput.readInt();
                if (utf == null) { // yey²?

                } else {
                    list[slot] = itemStackFromBase64((String) utf);
                }
            }

            dataInput.close();
            return list;
        }
        catch (Exception e) {
            throw new IllegalStateException("Unable to load item stacks.", e);
        }
    }


    public static String inventoryToBase64(Inventory inventory) {

        String serialized = "";
        for (org.bukkit.inventory.ItemStack itemStack : inventory.getContents()) {
            if (itemStack == null) serialized = serialized + Base64Coder.encodeString("null") + "&";
            else serialized = serialized + Base64Coder.encodeString(itemStackToBase64(itemStack)) + "&";
        }
        if (serialized.endsWith("&")) serialized = serialized.substring(0, serialized.length()-1);
        return serialized;

    }
    public static Inventory inventoryFromBase64(String data) {

        List<org.bukkit.inventory.ItemStack> itemStackList = new ArrayList<>();
        for (String part : data.split("&")) {
            String stackData = Base64Coder.decodeString(part);
            if (!stackData.equals("null")) {
                org.bukkit.inventory.ItemStack itemStack = itemStackFromBase64(stackData);
                itemStackList.add(itemStack);
            }
            else itemStackList.add(null);
        }

        Inventory inventory = Bukkit.getServer().createInventory(null, itemStackList.size());
        for (int i = 0; i < itemStackList.size(); i++) {
            inventory.setItem(i, itemStackList.get(i));
        }

        return inventory;

    }*/



    /*
     * Utils
     */
    private static Class<?> getNMSClass(String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("net.minecraft.server." + version + "." + name);
        } catch (ClassNotFoundException var3) {
            var3.printStackTrace();
            return null;
        }
    }
    private static Class<?> getOBClass(String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            return Class.forName("org.bukkit.craftbukkit." + version + "." + name);
        } catch (ClassNotFoundException var3) {
            var3.printStackTrace();
            return null;
        }
    }

}
