package stackunderflow.endersync.utils;


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/*
 * Some basic crypto logic.
 */
public class Crypto {

    public static final String ALGORITHM = "AES";
    public static byte[] KEY;


    /*
     * Encrypt a string.
     */
    public static String encrypt(String valueToEnc) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encValue = c.doFinal(valueToEnc.getBytes());
        return new BASE64Encoder().encode(encValue);
    }


    /*
     * Decrypt a string.
     */
    public static String decrypt(String encryptedValue) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
        byte[] decValue = c.doFinal(decordedValue);
        return new String(decValue);
    }


    /*
     * Generates a Key object.
     */
    private static Key generateKey() throws Exception {
        return new SecretKeySpec(Crypto.KEY, ALGORITHM);
    }

}
