package stackunderflow.endersync.utils;


import org.bukkit.entity.Player;
import stackunderflow.endersync.networking.DataHandler;
import stackunderflow.endersync.networking.Networking;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/*
 * Utilities for messaging between Bukkit & BungeeCord.
 */
public class Messaging {

    // ================================     VARS

    // References
    public static DataHandler INSTANCE;

    // State
    public static HashMap<UUID, CompletableFuture> occupiedPlayersCallbacks = new HashMap<>();


    /*
     * Asks BungeeCord whether a player is occupied and runs callback when response arrives.
     */
    public static CompletableFuture<Boolean> isPlayerOccupied(UUID playerUUID) {

        // Setup Future.
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        Messaging.occupiedPlayersCallbacks.put(playerUUID, completableFuture);

        // Request.
        Networking.INSTANCE.sendToBridge("IOP", playerUUID.toString());

        return completableFuture;

    }
    public static CompletableFuture<Boolean> isPlayerOccupied(Player player) {
        return isPlayerOccupied(player.getUniqueId());
    }

}
