package stackunderflow.endersync.utils;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;

import java.util.List;


/*
 * Handles string formatting which can be chained.
 * See method docs for more info.
 */
@Getter
@Setter
public class StringFormatter {

    // =============  VARS

    private String STR;

    private boolean useSuccessChar = false;
    private boolean successState = false;
    private boolean progressState = false;



    // =============  CONSTRUCTOR & Return logic

    public StringFormatter(String str) {
        setSTR(str);
    }


    /*
     * A save alternative to this.STR.
     */
    public String getSaveSTR() {
        if (this.STR == null) return "";
        return this.STR;
    }

    /*
     * Returns the final string.
     */
    public String getSTR() {

        // Replace prefix.
        this.prefix();

        // Replace formatting chars.
        this.formatters();

        return getSaveSTR();

    }


    /*
     * Sends the formatted string to the player.
     */
    public void sendMessageTo(Object recipient) {

        // RET: Message is disabled.
        if (getSaveSTR() == null || getSaveSTR().equals("")) return;

        if (recipient instanceof Player) {
            ((Player) recipient).sendMessage(getSTR());
        }
        if (recipient instanceof ConsoleCommandSender) {
            ((ConsoleCommandSender) recipient).sendMessage(getSTR());
        }
    }


    /*
     * Broadcasts the message to all players on the server.
     */
    public void broadcastMessage() {
        Broadcast.all(getSTR());
    }


    /*
     * Broadcast the message to all players except for one.
     */
    public void broadcastMessageExcept(Player player) {
        Broadcast.broadcastWithException(getSTR(), player);
    }


    /*
     * Broadcast the message to all players except for the ones in the exceptions list.
     */
    public void broadcastMessageExcepts(List<Player> exceptions) {
        Broadcast.broadcastWithExceptions(getSTR(), exceptions);
    }


    /*
     * Broadcast the message to all players on the list.
     */
    public void broadcastMessageTo(List<Player> players) {
        Broadcast.broadcastTo(getSTR(), players);
    }


    // =============  FORMATERS


    /*
     * Replacements:
     *      source               - with
     *
     */
    public StringFormatter replace(String source, String with) {
        if (getSTR().contains(source)) this.STR = getSTR().replace(source, with);
        return this;
    }


    /*
     * Replacements:
     *      {prefix}              - Main.INSTANCE.getPluginChatPrefix()
     *
     */
    public StringFormatter prefix() {

        if (getSaveSTR().contains("{prefix}")) {

            String prefix = Main.INSTANCE.getPluginChatPrefix();
            if (prefix.contains("{success}")) {

                String successInsert = "";
                if (isUseSuccessChar()) {
                    if (isSuccessState()) {
                        successInsert = "§a§l✔§R ";
                    }
                    else if (!isSuccessState() && isProgressState()) {
                        successInsert = "§b§l...§R "; // ↻
                    }
                    else {
                        successInsert = "§c§l✘§R ";
                    }
                }
                prefix = prefix.replace("{success}", successInsert);

            }
            this.STR = this.STR.replace("{prefix}", prefix);

        }
        return this;

    }


    /*
     * Replacements:
     *      &              - §
     *
     */
    public StringFormatter formatters() {
        this.replaceAll("&", "§");
        return this;

    }


    /*
     * Sets up the success state, so that it can later be replaced.
     *
     */
    public StringFormatter setSuccess(boolean success) {
        setUseSuccessChar(true);
        setSuccessState(success);
        return this;
    }

    public StringFormatter enableProgress() {
        setUseSuccessChar(true);
        setProgressState(true);
        return this;
    }


    /*
     * Replacements:
     *      {playerDisplayName}     - player.getDisplayName()
     *      {playerName}            - player.getName()
     *
     */
    public StringFormatter player(Player player) {

        if (getSaveSTR().contains("{playerDisplayName}")) this.STR = getSaveSTR().replace("{playerDisplayName}", player.getDisplayName());
        if (getSaveSTR().contains("{playerName}")) this.STR = getSaveSTR().replace("{playerName}", player.getName());

        return this;

    }


    /*
     * Replacements:
     *      {ownerDisplayName}      - account.getOwner()
     *      {currentBalance}        - account.getCurrentBalance()
     *      {currencySymbol}        - MoneyManager.INSTANCE.getCurrencySymbol()
     *
     */
    /*public StringFormatter playerMoneyAccount(PlayerAccount account) {

        if (this.STR.contains("{ownerDisplayName}")) this.STR = this.STR.replace("{ownerDisplayName}", account.getOwnerDisplayName());
        if (this.STR.contains("{currentBalance}")) this.STR = this.STR.replace("{currentBalance}", account.getCurrentBalanceString());
        if (this.STR.contains("{currencySymbol}")) this.STR = this.STR.replace("{currencySymbol}", MoneyManager.INSTANCE.getCurrencySymbol());

        return this;

    }*/


    /*
     * Replaces all occurrences in a string.
     */
    public StringFormatter replaceAll(String regex, String content) {
        if (getSaveSTR().contains(regex)) this.STR = getSaveSTR().replaceAll(regex, content);
        return this;
    }

}
