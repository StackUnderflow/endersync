package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Air
 */
@Getter
@Setter
public class AirPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public AirPlayerSyncModule() {

        // Setup SyncModule.
        super("air");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("air_max", "SMALLINT NOT NULL");
        getTableManager().addColumn("air_level", "SMALLINT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.air.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Sync data.
        player.setMaximumAir((int) row.get("air_max"));
        player.setRemainingAir((int) row.get("air_level"));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.air.enabled")) return true;

        // Save data.
        row.set("air_max", player.getMaximumAir());
        row.set("air_level", player.getRemainingAir());
        row.update();

        return true;

    }

}
