package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersync.database.mysql.TableManager;


/*
 * A common BaseClass.
 */
@Getter
@Setter
public class SyncModule {

    // ================================     VARS

    // References
    private TableManager tableManager;


    // Settings
    private String name = "default";
    private boolean valid = false;


    // State
    private boolean syncing = false;



    // ================================     CONSTRUCTOR

    public SyncModule() {}
    public SyncModule(String name) {
        setName(name);
        setTableManager(new TableManager("es_module_"+getName()));
    }



    // ================================     INTERNAL LOGIC

}
