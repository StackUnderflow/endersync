package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.Configuration;

import java.util.ArrayList;


/*
 * This sync module handles the following aspects:
 * - PotionEffects
 */
@Getter
@Setter
public class PotionEffectsPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public PotionEffectsPlayerSyncModule() {

        // Setup SyncModule.
        super("potion_effects");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("effects_encoded", "TEXT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.potionEffects.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Deserialize potionEffects.
        ArrayList<PotionEffect> potionEffects = Serializer.INSTANCE.getPotionEffectSerializer().DeserializePotionEffects((String) row.get("effects_encoded"));

        // RET: Deserialization error.
        if (potionEffects == null) {
            Main.INSTANCE.logError("Could not deserialize potionEffects!");
            return false;
        }

        // Sync data.
        for(PotionEffect effect : player.getActivePotionEffects())
            player.removePotionEffect(effect.getType());
        for (PotionEffect potionEffect : potionEffects) {
            potionEffect.apply(player);
        }

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.potionEffects.enabled")) return true;

        // Save data.
        row.set("effects_encoded", Serializer.INSTANCE.getPotionEffectSerializer().SerializePotionEffects(player.getActivePotionEffects()));
        row.update();

        return true;

    }

}
