package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Armor
 */
@Getter
@Setter
public class ArmorPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public ArmorPlayerSyncModule() {

        // Setup SyncModule.
        super("armor");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("armor_encoded", "TEXT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.armor.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Deserialize the data,
        String serialized = (String) row.get("armor_encoded");
        if (serialized.length()<=0 || serialized.equals("")) return true;
        String[] parts = serialized.split("&");
        if (!parts[0].equals("null")) player.getInventory().setHelmet(Serializer.INSTANCE.getItemStackSerializer().fromBase64(parts[0]));
        if (!parts[1].equals("null")) player.getInventory().setChestplate(Serializer.INSTANCE.getItemStackSerializer().fromBase64(parts[1]));
        if (!parts[2].equals("null")) player.getInventory().setLeggings(Serializer.INSTANCE.getItemStackSerializer().fromBase64(parts[2]));
        if (!parts[3].equals("null")) player.getInventory().setBoots(Serializer.INSTANCE.getItemStackSerializer().fromBase64(parts[3]));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.armor.enabled")) return true;

        // RET: Save when in creative is disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.inventory.saveWhenInCreative") && player.getGameMode().equals(GameMode.CREATIVE)) return true;

        PlayerInventory inv = player.getInventory();

        // Serialize items.
        String serializedHelment = "null";
        String serializedChestplate = "null";
        String serializedLeggins = "null";
        String serializedBoots = "null";

        if (inv.getHelmet() != null) serializedHelment = Serializer.INSTANCE.getItemStackSerializer().toBase64(inv.getHelmet());
        if (inv.getChestplate() != null) serializedChestplate = Serializer.INSTANCE.getItemStackSerializer().toBase64(inv.getChestplate());
        if (inv.getLeggings() != null) serializedLeggins = Serializer.INSTANCE.getItemStackSerializer().toBase64(inv.getLeggings());
        if (inv.getBoots() != null) serializedBoots = Serializer.INSTANCE.getItemStackSerializer().toBase64(inv.getBoots());

        String serialized = serializedHelment+"&"+serializedChestplate+"&"+serializedLeggins+"&"+serializedBoots;

        // Save data.
        row.set("armor_encoded", serialized);
        row.update();

        return true;

    }

}
