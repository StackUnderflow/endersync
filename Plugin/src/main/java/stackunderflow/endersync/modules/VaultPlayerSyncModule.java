package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.utils.Configuration;


/*
 * This sync module handles the following aspects:
 * - Vault economy
 */
@Getter
@Setter
public class VaultPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    private Economy economy;


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public VaultPlayerSyncModule(Economy economy) {

        // Setup SyncModule.
        super("vault_economy");

        // Reference vault api.
        setEconomy(economy);

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("balance_value", "DOUBLE NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.vault.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Sync data.
        if (Double.compare(getEconomy().getBalance(player), 0.0) <= 0) {
            getEconomy().depositPlayer(player, Math.abs(getEconomy().getBalance(player)));
        }
        else getEconomy().withdrawPlayer(player, getEconomy().getBalance(player));
        if (Double.compare((double)row.get("balance_value"), 0.0) <= 0) {
            getEconomy().withdrawPlayer(player, Math.abs((double) row.get("balance_value")));
        }
        else getEconomy().depositPlayer(player, (double) row.get("balance_value"));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.vault.enabled")) return true;

        // Save data.
        row.set("balance_value", getEconomy().getBalance(player));
        row.update();

        return true;

    }

}
