package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.RowNotFoundException;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.Configuration;

import java.sql.SQLException;
import java.util.UUID;


/*
 * This sync module handles the following aspects:
 * - Enderchest
 */
@Getter
@Setter
public class EnderChestPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public EnderChestPlayerSyncModule() {

        // Setup SyncModule.
        super("enderchest");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("inventory_encoded", "TEXT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.enderchest.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Deserialize the inventory.
        Inventory inventory = Serializer.INSTANCE.getInventorySerializer().inventoryFromBase64((String) row.get("inventory_encoded"), "", InventoryType.ENDER_CHEST);

        // RET: No inv.
        if (inventory == null) return false;

        // Sync data.
        player.getEnderChest().setContents(inventory.getContents());

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.enderchest.enabled")) return true;

        // Save data.
        row.set("inventory_encoded", Serializer.INSTANCE.getInventorySerializer().inventoryToBase64(player.getEnderChest()));
        row.update();

        return true;

    }


    /*
     * Set the contents of a players enderchest.
     */
    public boolean setPlayerEnderChestInventory(UUID playerUUID, Inventory inventory) {

        // Get the row.
        Row row;
        try {
            row = getPlayerRow(playerUUID);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } catch (RowNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        // Update.
        row.set("inventory_encoded", Serializer.INSTANCE.getInventorySerializer().inventoryToBase64(inventory));
        row.update();

        return true;

    }


    /*
     * Gets the enderchest inventory of a player.
     */
    public Inventory getPlayerEnderChestInventory(UUID playerUUID) {

        // Get the row.
        Row row;
        try {
            row = getPlayerRow(playerUUID);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (RowNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        // Deserialize the inventory.
        Inventory inventory = Serializer.INSTANCE.getInventorySerializer().inventoryFromBase64((String) row.get("inventory_encoded"), "", InventoryType.ENDER_CHEST);
        return inventory;

    }

}
