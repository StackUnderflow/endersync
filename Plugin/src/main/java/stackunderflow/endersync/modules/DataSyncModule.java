package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.database.mysql.TableManager;
import stackunderflow.endersync.exceptions.ModuleExceptions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/*
 * The base class for all sync modules which can be extended to
 * create custom modules which can sync player unrelated data.
 */
@Getter
@Setter
public class DataSyncModule extends SyncModule {
    // TODO: IMPLEMENT
}
