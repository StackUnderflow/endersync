package stackunderflow.endersync.modules;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.RowNotFoundException;
import stackunderflow.endersync.serializers.Serializer;
import stackunderflow.endersync.utils.Configuration;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;


/*
 * This sync module handles the following aspects:
 * - Inventory
 */
@Getter
@Setter
public class InventoryPlayerSyncModule extends PlayerSyncModule {

    // ================================     VARS

    // References
    // Foo


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public InventoryPlayerSyncModule() {

        // Setup SyncModule.
        super("inventory");

        // Setup database tables.
        getTableManager().addColumn("player_uuid", "VARCHAR(36) NOT NULL UNIQUE");
        getTableManager().addColumn("inventory_encoded", "TEXT NOT NULL");
        getTableManager().addColumn("hotbar_slot", "TINYINT NOT NULL");

        // Finish the init. ! This needs to be here!
        finishModuleInit();

    }



    // ================================     LOGIC


    @Override
    public void blockPlayer(Player player) {
        super.blockPlayer(player);
    }


    @Override
    public void unblockPlayer(Player player) {
        super.unblockPlayer(player);
    }


    /*
     * Implement logic to sync player data.
     */
    @Override
    public boolean onPlayerSync(Row row, Player player) {

        // RET: Sync disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.sync.inventory.enabled")) return true;

        // RET: No entry found: Call save & Return.
        if (row.isEmptyIgnorePlayerData()) { onPlayerSave(row, player); return true; }

        // Deserialize the inventory.
        Inventory inventory = Serializer.INSTANCE.getInventorySerializer().inventoryFromBase64((String) row.get("inventory_encoded"), "", InventoryType.PLAYER);

        // RET: No inv.
        if (inventory == null) return false;

        // Sync data.
        player.getInventory().setContents(inventory.getContents());
        player.getInventory().setHeldItemSlot((int) row.get("hotbar_slot"));

        return true;

    }


    /*
     * Implement logic to save player data.
     */
    @Override
    public boolean onPlayerSave(Row row, Player player) {

        // RET: Save disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.inventory.enabled")) return true;

        // RET: Sync when in creative is disabled.
        if (!Configuration.INSTANCE.get("features").getBoolean("features.save.inventory.saveWhenInCreative") && player.getGameMode().equals(GameMode.CREATIVE)) return true;

        Inventory playerInventory = player.getInventory();


        // RET: Adaptive mode not enabled.
        System.out.println(Configuration.INSTANCE.get("features").getString("features.save.inventory.adaptiveMode"));
        if (Configuration.INSTANCE.get("features").getString("features.save.inventory.adaptiveMode").equalsIgnoreCase("WHITELIST")) {

            // Get items on whitelist.
            List<String> whitelistItems = Configuration.INSTANCE.get("features").getStringList("features.save.inventory.whitelist");


            // Check all items in inventory and remove from inventory if not on whitelist.
            int i = 0;
            for (ItemStack itemStack : playerInventory.getContents()) {
                if (itemStack != null) {
                    if (!whitelistItems.contains(itemStack.getType().name())) {
                        playerInventory.setItem(i, null);
                    }
                }
                i++;
            }

        }


        // Save data.
        row.set("inventory_encoded", Serializer.INSTANCE.getInventorySerializer().inventoryToBase64(playerInventory));
        row.set("hotbar_slot", player.getInventory().getHeldItemSlot());
        row.update();

        return true;

    }


    /*
     * Set the contents of a players inventory.
     */
    public boolean setPlayerInventory(UUID playerUUID, Inventory inventory) {

        // Get the row.
        Row row;
        try {
            row = getPlayerRow(playerUUID);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } catch (RowNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        // Update.
        row.set("inventory_encoded", Serializer.INSTANCE.getInventorySerializer().inventoryToBase64(inventory));
        row.update();

        return true;

    }


    /*
     * Gets the inventory of a player.
     */
    public Inventory getPlayerInventory(UUID playerUUID) {

        // Get the row.
        Row row;
        try {
            row = getPlayerRow(playerUUID);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } catch (RowNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        // Deserialize the inventory.
        Inventory inventory = Serializer.INSTANCE.getInventorySerializer().inventoryFromBase64((String) row.get("inventory_encoded"), "", InventoryType.PLAYER);
        return inventory;

    }

}
