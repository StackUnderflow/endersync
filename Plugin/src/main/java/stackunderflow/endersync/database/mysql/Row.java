package stackunderflow.endersync.database.mysql;

import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersync.serializers.Serializer;

import java.util.HashMap;
import java.util.UUID;


/*
 * Represents a row in a table.
 */
@Getter
@Setter
public class Row {

    // ================================     VARS

    // References
    private HashMap<String, Object> values;
    private TableManager tableManager;


    // Settings
    // Foo




    // ================================     CONSTRUCTOR

    public Row(TableManager tableManager) {
        setValues(new HashMap<>());
        setTableManager(tableManager);

        // Automatically generate an id.
        // This will be overridden if the row is fetched from the db.
        set("id", UUID.randomUUID());
    }



    // ================================     LOGIC


    /*
     * Returns data in column if present.
     * -> User needs to cast the object to the correct data type.
     */
    public Object get(String columnName) {
        return getValues().get(columnName);
    }


    /*
     * Update the value of a column.
     */
    public void set(String columnName, Object value) {

        // Update value.
        getValues().remove(columnName);

        // Handle booleans.
        if (value instanceof Boolean) {
            getValues().put(columnName, Serializer.INSTANCE.getGenericSerializer().SerializeBoolean((boolean) value));
        }
        else {
            getValues().put(columnName, value);
        }

    }


    /*
     * Returns whether the row is empty or not.
     * This does not include the "id" column.
     */
    public boolean isEmpty() {
        return !(getValues().keySet().size() > 1);
    }


    /*
     * Returns whether the row is empty or not.
     * This does not include the "id" column and other user info.
     */
    public boolean isEmptyIgnorePlayerData() {
        return !(getValues().keySet().size() > 3);
    }


    /*
     * Updates the row in the database.
     */
    public boolean update() {
        return getTableManager().insertOrUpdateRow(this);
    }


    /*
     * Deletes the row.
     */
    public boolean delete() { return getTableManager().deleteRow(this); }

}
