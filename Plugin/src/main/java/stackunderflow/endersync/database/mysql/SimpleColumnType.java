package stackunderflow.endersync.database.mysql;


/*
 * Simple data types which will get converted into real, predefined MySQL
 * data types.
 */
public enum SimpleColumnType {
    INT,
    STRING,
    BOOLEAN
}
