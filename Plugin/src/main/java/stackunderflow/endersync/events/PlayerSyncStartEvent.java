package stackunderflow.endersync.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;



/*
 * This event is called when a sync is about to be started.
 * It can be canceled to prevent the sync.
 */
@Getter
@Setter
public class PlayerSyncStartEvent extends Event implements Cancellable {

    // ================================     VARS

    // References
    private Player player;

    // State
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();




    // ================================     CONSTRUCTOR

    public PlayerSyncStartEvent(Player player) {
        setPlayer(player);
        setCancelled(false);
    }


    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean arg0) {
        this.cancelled = arg0;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
