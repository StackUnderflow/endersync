package stackunderflow.endersync.events;


import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
 * This event is called when the plugin wants to save player data.
 * It can be canceled to prevent the save.
 */
@Getter
@Setter
public class PlayerSaveStartEvent extends Event implements Cancellable {

    // ================================     VARS

    // References
    private Player player;

    // State
    private boolean cancelled;
    private static final HandlerList handlers = new HandlerList();




    // ================================     CONSTRUCTOR

    public PlayerSaveStartEvent(Player player) {
        setPlayer(player);
        setCancelled(false);
    }


    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean arg0) {
        this.cancelled = arg0;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
