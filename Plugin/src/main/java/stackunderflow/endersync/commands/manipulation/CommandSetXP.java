package stackunderflow.endersync.commands.manipulation;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The player can set other players xp.
 */
@Getter
@Setter
public class CommandSetXP extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandSetXP(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("setXp")
                .addToken("<name>")
                .addToken("<amount>");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.setXp")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Info.
        setPlayerXp(sender, args.get("<name>").getValue(), args.get("<amount>").getValue());

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Info.
        setPlayerXp(sender, args.get("<name>").getValue(), args.get("<amount>").getValue());

    }


    private void setPlayerXp(CommandSender sender, String target, String amount) {

        // Parse amount.
        int amountValue = 0;
        try {
            amountValue = Integer.parseInt(amount);
        }
        catch (NumberFormatException e) {
            sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdSetXpFailed"))
                    .setSuccess(false)
                    .getSTR()
            );
            return;
        }

        // Get the inventory row.
        /*Row row;
        try {
            row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(target), "experience");
        } catch (SQLException e) {
            sender.sendMessage(new StringFormatter(Config.INSTANCE.getMessage("generalError"))
                    .getSTR())
            ;
            e.printStackTrace();
            return;
        }

        row.set("exp_total_experience", amountValue);
        row.update();

        // Sync the target player if he is online.
        if (Bukkit.getPlayer(target) != null) {
            SyncManager.INSTANCE.startPlayerSync(Bukkit.getPlayer(target), true);
        }

        sender.sendMessage(new StringFormatter(Config.INSTANCE.getMessage("cmdSetXpSuccess"))
                .replace("{xp}", Integer.toString((int) row.get("exp_total_experience")))
                .replace("{player}", target)
                .setSuccess(true)
                .getSTR()
        );*/

    }

}
