package stackunderflow.endersync.commands.manipulation;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.ModuleExceptions;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;


/*
 * The player can view other players vault balances.
 */
@Getter
@Setter
public class CommandEcoBal extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandEcoBal(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("ecoBal")
                .addToken("<name>");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.ecoBal")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Info.
        displayPlayerBalance(sender, args.get("<name>").getValue());

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Info.
        displayPlayerBalance(sender, args.get("<name>").getValue());

    }



    private void displayPlayerBalance(CommandSender sender, String target) {

        /*try {

            final PlayerSyncModule vaultModule = (PlayerSyncModule) SyncManager.INSTANCE.getModule("vault_economy");

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                // Get the vault row.
                Row row;
                try {

                    UUID targetUuid = UUIDUtil.getUUID(target);
                    if (targetUuid == null) {
                        new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.playerNotFound"))
                                .replace("{player}", target).sendMessageTo(sender);
                        return;
                    }

                    row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(targetUuid), vaultModule);
                } catch (SQLException | DatabaseExceptions.RowNotFoundException e) {
                    sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.generalError"))
                            .getSTR())
                    ;
                    e.printStackTrace();
                    return;
                }

                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdEcoBalSuccess"))
                        .replace("{balance}", Double.toString((double) row.get("balance_value")))
                        .replace("{player}", target)
                        .getSTR()
                );

            }, 20L);

        }
        catch (ModuleExceptions.ModuleNotFoundException e) {
            // TODO: Add module not enabled player message..
            return;
        }*/

    }

}
