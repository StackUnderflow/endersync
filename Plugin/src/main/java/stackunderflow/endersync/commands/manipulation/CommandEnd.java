package stackunderflow.endersync.commands.manipulation;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.exceptions.ModuleNotFoundException;
import stackunderflow.endersync.exceptions.NonePlayersOnline;
import stackunderflow.endersync.modules.EnderChestPlayerSyncModule;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/*
 * This command lets player view other players enderchest's & manipulate them.
 */
@Getter
@Setter
public class CommandEnd extends Command {

    // ================================     VARS

    public static HashMap<UUID, UUID> activeInvseePlayer;



    // ================================     CONSTRUCTOR


    public CommandEnd(String name) {
        super(name);

        activeInvseePlayer = new HashMap<>();

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("end")
                .addToken("<name>");

    }



    // ================================     COMMAND LOGIC


    @Override
    @SuppressWarnings("deprecation")
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.end")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Get target player UUID.
        UUID targetPlayer = UUIDUtil.getUUID(args.get("<name>").getValue());

        // RET: Player does not exist.
        if (targetPlayer == null) {
            new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdEndNoPlayerDataFound"))
                    .replace("{player}", args.get("<name>").getValue()).sendMessageTo(sender);
            return;
        }


        // Save the target player.
        try {
            SyncManager.INSTANCE.savePlayer(targetPlayer, (PlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest"), new Promise() {
                @Override
                public void onSuccess() {

                    // Schedule open inv.
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.INSTANCE, () -> {

                        // Get the inventory.
                        Inventory inventory = null;
                        try {
                            inventory = ((EnderChestPlayerSyncModule) SyncManager.INSTANCE.getModule("enderchest")).getPlayerEnderChestInventory(targetPlayer);
                        } catch (ModuleNotFoundException e) {
                            e.printStackTrace();
                        }

                        activeInvseePlayer.put(sender.getUniqueId(), targetPlayer);
                        sender.openInventory(inventory);

                    }, 20L);

                }


                @Override
                public void onError(Exception ex) {

                }
            });
        } catch (ModuleNotFoundException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {
        sender.sendMessage(
                new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.consoleCannotExecute")).getSTR()
        );
    }

}
