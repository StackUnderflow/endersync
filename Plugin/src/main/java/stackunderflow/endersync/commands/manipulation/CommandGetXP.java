package stackunderflow.endersync.commands.manipulation;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The player can view other players xp.
 */
@Getter
@Setter
public class CommandGetXP extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandGetXP(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("getXp")
                .addToken("<name>");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.getXp")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Info.
        displayPlayerXp(sender, args.get("<name>").getValue());

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Info.
        displayPlayerXp(sender, args.get("<name>").getValue());

    }



    private void displayPlayerXp(CommandSender sender, String target) {

        // Get the inventory row.
        /*Row row;
        try {
            row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(target), "experience");
        } catch (SQLException e) {
            sender.sendMessage(new StringFormatter(Config.INSTANCE.getMessage("generalError"))
                    .getSTR())
            ;
            e.printStackTrace();
            return;
        }

        sender.sendMessage(new StringFormatter(Config.INSTANCE.getMessage("cmdGetXpSuccess"))
                .replace("{xp}", Integer.toString((int) row.get("exp_total_experience")))
                .replace("{player}", target)
                .getSTR()
        );*/

    }

}
