package stackunderflow.endersync.commands.manipulation;

import co.aikar.taskchain.TaskChain;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandSaveAndKick extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandSaveAndKick(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("saveAndKick");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.saveAndKick")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        saveAndKickAll(sender);

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        saveAndKickAll(sender);

    }


    private void saveAndKickAll(CommandSender sender) {

        TaskChain<?> chain = Main.newChain();
        chain
                .sync(() -> {

                    // Save all.
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        SyncManager.INSTANCE.savePlayer(player, new Promise() {
                            @Override
                            public void onSuccess() {}
                            @Override
                            public void onError(Exception ex) {}
                        });
                    }

                })
                .sync(() -> {

                    // Kick all.
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.kickPlayer(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.kickReasonCmdSaveAndKick")).getSTR());
                    }

                })
                .execute();

    }

}
