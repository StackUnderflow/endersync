package stackunderflow.endersync.commands.administration;

import co.aikar.taskchain.TaskChain;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import stackunderflow.endersync.Main;
import stackunderflow.endersync.SyncManager;
import stackunderflow.endersync.database.mysql.Row;
import stackunderflow.endersync.exceptions.RowNotFoundException;
import stackunderflow.endersync.modules.PlayerSyncModule;
import stackunderflow.endersync.utils.*;
import stackunderflow.libs.commandfactory.Command;
import stackunderflow.libs.commandfactory.CommandArg;

import java.sql.SQLException;
import java.util.Map;


/*
 * The skin command which can be used to change other players or one own's skin.
 */
@Getter
@Setter
public class CommandDelete extends Command {

    // ================================     VARS

    // Null



    // ================================     CONSTRUCTOR


    public CommandDelete(String name) {
        super(name);

        // Setup tokens.
        this
                .addToken("endersync", "es")
                .addToken("delete")
                .addToken("<name>");

    }



    // ================================     COMMAND LOGIC


    @Override
    public void onPlayerCall(Player sender, Map<String, CommandArg> args) {

        // RET: No permission
        if (!BukkitUtils.playerHasPermission(sender, "endersync.admin.delete")) {
            if (Configuration.INSTANCE.get().getBoolean("plugin.displayErrorMessagesToPlayers")) {
                sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("general.noPermission"))
                        .getSTR()
                );
            }
            return;
        }

        // Delete.
        deletePlayer(sender, args.get("<name>").getValue());

    }


    @Override
    public void onConsoleCall(CommandSender sender, Map<String, CommandArg> args) {

        // Delete.
        deletePlayer(sender, args.get("<name>").getValue());

    }



    private void deletePlayer(CommandSender sender, String name) {

        // Info.
        sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdDeleteStarted"))
                .replace("{player}", name)
                .enableProgress()
                .getSTR()
        );

        // Loop through all active modules.
        TaskChain<?> chain = Main.newChain();
        for (PlayerSyncModule module : SyncManager.INSTANCE.getActivePlayerModules().values()) {

            chain.async(() -> {
                Row row;
                try {
                    row = Main.API.getPlayerDataRow(Bukkit.getOfflinePlayer(UUIDUtil.getUUID(name)), module);
                } catch (SQLException | RowNotFoundException e) {
                    e.printStackTrace();
                    return;
                }
                row.delete();
            });

        }
        chain.sync(() -> {

            // Info.
            sender.sendMessage(new StringFormatter(Configuration.INSTANCE.getLocalizedMessage("command.cmdDeleteSuccess"))
                    .replace("{player}", name)
                    .setSuccess(true)
                    .getSTR()
            );

        });
        chain.execute();

    }

}
